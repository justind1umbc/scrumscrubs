The parser uses the Lark library.
https://github.com/lark-parser/lark
pip install lark-parser

To generate the preference list from the constructed string, call generate_preferences(prefs)

The parser parses from a modified EBNF grammar as follows
schedule: (employee _NEWLINE (preference _NEWLINE)+)+
employee: "employee"i (string|int) status
status: (/FTE/ (number))|/ML/
preference: type modifier? (date|(shift date)) priority?
type: "must"i|"prefer"i
modifier: "not"i
shift: "shift"i (/[1-3]/)
date: "date"i calendar [("repeat"i weekdays)? ("to"i calendar)]
priority: "1".."5"
calendar: month "/" day "/" year
month: (/1[0-2]/)|/[1-9]/
day: (/[1-2][0-9]/)|(/3[0-1]/)|/[1-9]/
year: /[0-9]/~4
weekdays: "m"i? "tu"i? "w"i? "th"i? "f"i? "sa"i? "su"i?

Which doesn't mean much without knowing what EBNF is.
A schedule is a sequence of employee preferences specified by the the line:
employee "name" status 
or
employee ID status
where "name" is a quoted string representing the employee's name, ID is the employee's identification number, and status describes their FTE or status as an ML
After defining an employee, preferences are listed line by line. A preference is defined by its type (prefer for soft preferences and must for hard preferences) followed by a modifier which will invert the preference if used, followed by a date or a shift and a date, followed by a priority. 

a shift is specified by typing
shift
followed by the number corresponding to the shift, being 1, 2, or 3.

A date can be a single calendar date, or a range of calendar dates.
date 10/11/2012
will include only the provided date in the preference
date 10/11/2012 to 11/12/2012
will include every date in the provided range in the preference
date 10/11/2012 repeat m w to 11/12/2012
will include every monday and wednesday starting from the given date until the ending date. It will not include the starting date if it does not fall on the given weekday. 
Whenever an ending date is supplied, the ending date will not be included in the preference.

Priority is a single number in the range 1 to 5. Priority is ignored for hard preferences.

preferences are applied to a given employee until a new employee is provided.

The generated preference list will be of the format
[([employee, [status]], [[preference], ... , [preference]]), ..., ([employee, [status]], ...)]
That is, each element in the top level list is a tuple containing an employee followed by the list of preferences. Each individual preference contains all the information provided in the line with date converted to a set of datetime.date containing the dates that are relevant to the given preference.
Sets are used so that set operations can be used to create a full list of date preferences. The parser does not know the date range to consider for scheduling, so this set concatenation is not performed in the parser.