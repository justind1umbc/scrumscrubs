from lark import Lark
from lark import Transformer
import datetime

parser = Lark(r"""
	schedule: (preferences)+
	preferences: employee _NEWLINE (preference _NEWLINE)+
	employee: "employee"i (string|int) status
	status: (/FTE/ (number))|/ML/
	preference: type modifier? (date|(shift date)) priority?
	!type: "must"i|"prefer"i
	!modifier: "not"i
	shift: "shift"i (/[1-3]/)
	date: "date"i calendar [("repeat"i weekdays)? ("to"i calendar)]
	priority: "1".."5"
	calendar: month "/" day "/" year
	month: (/1[0-2]/)|/[1-9]/
	day: (/[1-2][0-9]/)|(/3[0-1]/)|/[1-9]/
	year: /[0-9]/~4
	!weekdays: "m"i? "tu"i? "w"i? "th"i? "f"i? "sa"i? "su"i?
	
	string: ESCAPED_STRING
	number: NUMBER
	int: INT
	
	%import common.ESCAPED_STRING
	%import common.NUMBER
	%import common.NEWLINE -> _NEWLINE
	%import common.INT
	%import common.WS_INLINE
	%ignore WS_INLINE
""", start="schedule")


class ScheduleTransformer(Transformer):
	def schedule(self, schedule):
		return schedule

	def preferences(self, preferences):
		return (preferences[0], preferences[1:])

	def employee(self, employee):
		return employee

	def status(self, status):
		return [status[0].value, status[1]]

	def preference(self, preference):
		return preference


	def type(self, type):
		return type[0].value

	def modifier(self, mod):
		return mod[0].lower()

	def shift(self, shift):
		return int(shift[0])

	# Unpack line into set of dates
	def date(self, date):
		#print(date)
		dates = set()
		if len(date) == 1:
			dates.add(date[0])
		elif len(date) == 2:
			start = date[0]
			end = date[1]
			for i in range(int((end - start).days)):
				dates.add(start + datetime.timedelta(i))
		elif len(date) == 3:  # Should be implied
			start = date[0]
			weekdays = date[1]
			end = date[2]
			for i in range(int((end - start).days)):
				day = start + datetime.timedelta(i)
				if day.weekday() in weekdays:
					dates.add(day)

		return dates

	def priority(self, priority):
		return int(priority[0])

	def calendar(self, calendar):
		month, day, year = calendar
		return datetime.date(year, month, day)

	def month(self, m):
		return int(m[0])

	def day(self, d):
		return int(d[0])

	def year(self, y):
		return int(y[0] + y[1] + y[2] + y[3])  # Super lazy concatenation

	def weekdays(self, weekdays):
		return [["m", "tu", "w", "th", "f", "sa", "su"].index(day.lower()) for day in weekdays]

	def string(self, s):
		return s[0][1:-1]

	def int(self, i):
		return int(i[0])

	def number(self, n):
		return float(n[0])


def generate_preferences(prefs):
	return ScheduleTransformer().transform(parser.parse(prefs))


print(generate_preferences(open("test.txt").read()))
