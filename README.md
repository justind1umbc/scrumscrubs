Scrum Scrubs Project Repo
=========================

I created an extra level of subdirs so that separate applications can be
created for testing purposes.

Subdirs:
--------

- **scrubsched** is for testing the core AI (heuristic) algorithm
- **???** is for testing the various widgets
- **scrubdb** is for testing the database
