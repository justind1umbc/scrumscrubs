import os
import sqlite3
import sys

# Tester has to pass in the database
if len(sys.argv) < 2:
    print("error: provide database filename as argument")
    exit(2)

conn = sqlite3.connect(sys.argv[1])
c = conn.cursor()

print(os.path.isfile(sys.argv[1]))

# Create few records in this table
c.execute("insert into Employee values (1, 1, 'Miranda', 0, 0, 'No')")
c.execute("insert into Employee values (2, 2, 'Felipe', 0, 0, 'No')")
c.execute("insert into Employee values (3, 3, 'Justin', 0, 0, 'No')")
c.execute("insert into Employee values (4, 1, 'Cameron', 0, 0, 'No')")
c.execute("insert into Employee values (5, 2, 'Fiona', 0, 0, 'No')")

c.execute("insert into Role values (1, 'Doctor')")
c.execute("insert into Role values (2, 'PA')")
c.execute("insert into Role values (3, 'Nurse')")

c.execute("insert into Schedule values (1, 12, 3, '5/19/2019', '07/14/2019')")

c.execute("insert into RequiredShifts values (1, 1, 3, 'Weekdays', 54, 'No')")

c.execute("insert into ScheduledShifts values (1, 1, 1, 1, 0, 0, 1, 0, 1, 1)")
c.execute("insert into ScheduledShifts values (2, 2, 1, 0, 1, 0, 1, 0, 2, 1)")
c.execute("insert into ScheduledShifts values (3, 3, 2, 0, 0, 1, 0, 1, 3, 1)")
c.execute("insert into ScheduledShifts values (4, 1, 2, 1, 0, 0, 1, 0, 4, 1)")
c.execute("insert into ScheduledShifts values (5, 2, 3, 0, 1, 0, 1, 0, 5, 1)")
c.execute("insert into ScheduledShifts values (6, 3, 3, 0, 0, 1, 0, 1, 1, 1)")
c.execute("insert into ScheduledShifts values (7, 1, 3, 1, 0, 0, 1, 0, 2, 1)")
c.execute("insert into ScheduledShifts values (8, 2, 1, 0, 1, 0, 1, 0, 3, 1)")
c.execute("insert into ScheduledShifts values (9, 3, 2, 0, 0, 1, 0, 1, 4, 1)")
c.execute("insert into ScheduledShifts values (10, 1, 2, 1, 0, 0, 1, 0, 5, 1)")

""" Get relevant information about an employee, in order via their role id, then their name. """
c.execute("select * from Employee group by roleID, name")

""" Query used by Felipe's code to get a list of shifts to write to the file. """
c.execute("select employeeID, dayIndex, shiftIndex from ScheduledShifts")

# Get the start date.
c.execute("select startDate from Schedule")

# Get employee name assigned to each scheduled shift.
c.execute("select dayIndex, shiftIndex, name from ScheduledShifts inner join Employee on Employee.employeeID = ScheduledShifts.employeeID group by dayIndex, shiftIndex, name")

# Get the possible roles for the system.
c.execute("select name from Role group by roleid")

# commit the executions
conn.commit()

# Close the connection
conn.close()
