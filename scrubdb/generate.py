import sqlite3
import sys

if len(sys.argv) < 2:
    print("error: provide database filename as argument")
    exit(2)

conn = sqlite3.connect(sys.argv[1])
c = conn.cursor()

work_table = [
    # week 1
    [["fiona", "justin", "miranda" ],   #SUN
     ["fiona", "justin", "miranda" ],   #MON
     ["felipe", "cameron", "fiona" ],   #TUE
     ["felipe", "cameron", "miranda"],  #WED
     ["miranda", "justin", "fiona" ],   #THU
     ["miranda", "fiona", "felipe" ],   #FRI
     ["justin", "cameron", "felipe" ]], #SAT
    [["justin", "fiona", "" ],   #SUN
     ["justin", "fiona", "miranda" ],   #MON
     ["felipe", "cameron", "fiona" ],   #TUE
     ["", "cameron", "miranda"],  #WED
     ["miranda", "fiona", "justin" ],   #THU
     ["fiona", "miranda", "felipe" ],   #FRI
     ["justin", "felipe", "cameron" ]]]

def triplify(table):
    triples = []
    for w in range(len(table)):
        week = table[w]
        for d in range(len(week)):
            day = week[d]
            for s in range(len(day)):
                triples.append((w*7+d, s, day[s]))
    return set(triples)

c.execute("INSERT INTO Role (roleID, name) VALUES (1, 'Student')")

employees = {'justin':1, 'miranda':2, 'fiona':3, 'felipe':4, 'cameron':5}
for (name, dbid) in employees.items():
    c.execute("INSERT INTO Employee (employeeID, roleID, name) VALUES (?, 1, ?)",
              (dbid, name))

c.execute("INSERT INTO Schedule (weekCount, startDate, endDate) VALUES (2, '4/29/19', '5/10/19')")
schedid = c.lastrowid

for (d,s,e) in sorted(triplify(work_table), key=lambda x: (x[0], x[1])):
    if(e == ""): continue
    c.execute("""
    INSERT INTO ScheduledShifts (scheduleID, dayIndex, shiftIndex, employeeID)
    VALUES (?,?,?,?)
    """, (schedid, d, s, employees[e]))

shiftreqs = [2,3,3,3,3,3,2]
for dow in range(len(shiftreqs)):
    c.execute("""
    INSERT INTO RequiredShifts (requiredShiftID, dayIndex, shiftName, numRequiredShifts)
    VALUES (?, ?, '', ?)
    """, (dow+1, dow+1, shiftreqs[dow]))

conn.commit()
