Create database
===============

This will *delete* all data currently in `scrubsched.db` and fill it with
test data:

```
sqlite3 scrubsched.db <schema.sql
python3 generate.py scrubsched.db
```

Install database
================

Now copy scrubsched.db to the scrubsched resource/base folder.

```
cp scrubsched.db ../scrubsched/src/main/resources/base/
```
