DROP TABLE IF EXISTS Employee;
CREATE TABLE Employee (
  employeeID INTEGER PRIMARY KEY UNIQUE NOT NULL,
  roleID INTEGER REFERENCES Role (roleID) ON DELETE CASCADE,
  name TEXT NOT NULL,
  hourTotal INTEGER NOT NULL DEFAULT (0),
  shiftTotal INTEGER NOT NULL DEFAULT (0),
  attendingStatus TEXT DEFAULT "No"
);

DROP TABLE IF EXISTS Role;
CREATE TABLE Role (
  roleID INTEGER PRIMARY KEY,
  name TEXT NOT NULL
);

DROP TABLE IF EXISTS Constraints;
CREATE TABLE Constraints (
  constraintId INTEGER PRIMARY KEY,
  shiftIndex INTEGER,
  dayIndex INTEGER,
  dateCannotWork TEXT,
  negate INTEGER NOT NULL DEFAULT (0),
  constraintScore INTEGER,
  employeeID INTEGER REFERENCES Employee (employeeID) ON DELETE CASCADE
);

DROP TABLE IF EXISTS ScheduledShifts;
CREATE TABLE ScheduledShifts (
  scheduledShiftID INTEGER PRIMARY KEY,
  shiftIndex INTEGER NOT NULL,
  dayIndex INTEGER NOT NULL,
  morningShiftCount INTEGER NOT NULL DEFAULT (0),
  afternoonShiftCount INTEGER NOT NULL DEFAULT (0),
  nightShiftCount INTEGER DEFAULT (0) NOT NULL,
  weekdayShiftCount INTEGER NOT NULL DEFAULT (0),
  weekendShiftCount INTEGER NOT NULL DEFAULT (0),
  employeeID INTEGER REFERENCES Employee (employeeID) ON DELETE CASCADE,
  scheduleID INTEGER REFERENCES Schedule (scheduleID) ON DELETE CASCADE
);

DROP TABLE IF EXISTS Schedule;
CREATE TABLE Schedule (
  scheduleID INTEGER PRIMARY KEY UNIQUE NOT NULL,
  weekCount INTEGER,
  monthInterval INTEGER NOT NULL DEFAULT (3),
  startDate TEXT NOT NULL,
  endDate TEXT NOT NULL
);

DROP TABLE IF EXISTS RequiredShifts;
CREATE TABLE RequiredShifts (
  requiredShiftID INTEGER PRIMARY KEY,
  dayIndex INTEGER NOT NULL,
  shiftTotalRequirement INTEGER NOT NULL DEFAULT (0),
  shiftName TEXT NOT NULL,
  numRequiredShifts INTEGER NOT NULL DEFAULT (0),
  attendingWeek TEXT
);
