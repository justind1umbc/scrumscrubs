import subprocess as subproc
import re

def tablize(work):
    sched = [(int(d), int(s), p) for (d,s,p) in work]
    table = []
    maxweek = max([int(d/7 + 1) for (d,_,_) in sched])
    maxshift = max([int(s) for (_,s,_) in sched])

    for y in range(maxweek*maxshift):
        table.append(["" for _ in range(7)])
    for (d,s,p) in sched:
        w = int((d-1)/7)
        table[w*maxshift + (s-1)][(d-1) % 7] = p
    return table

def run(script):
    proc = subproc.run(["clingo", script],
                       capture_output=True,
                       encoding="utf8")
    work = []
    workre = 'work\((\d+),(\d+),([^)]+)\)'
    lines = proc.stdout.split("\n")
    for i in range(len(lines)):
        if not re.match('Answer: (\d+)', lines[i]): continue
        work = [m.groups() for m in re.finditer(workre, lines[i+1])]

    assert len(work) > 0, "failed to extract answer"
    #answer = "work(4,2,justin) work(5,3,justin) work(6,3,justin) work(2,3,miranda) work(3,2,miranda) work(4,3,miranda) work(6,1,miranda) work(7,1,fiona) work(2,1,fiona) work(3,3,fiona) work(4,1,fiona) work(5,1,fiona) work(6,2,fiona) work(1,2,cameron) work(7,2,cameron) work(1,1,fellipe) work(2,2,fellipe) work(3,1,fellipe) work(5,2,fellipe)"
    #work = [m.groups() for m in re.finditer('work\((\d+),(\d+),([^)]+)\)', answer)]
    #print(work)

    #print(tablize(work))
    days = "SUN MON TUE WED THU FRI SAT".split(" ")
    table = tablize(work)
    print("\t".join(days))
    for col in table:
        print("\t".join(col))

    shiftn = {}
    #print("DAY\tDOW\tSHIFT\tPERSON")
    work = sorted(work, key=lambda x: (int(x[0]), int(x[1])))
    for w in work:
        (d, s, p) = w
        d = int(d)
        s = int(s)
        # dow = days[d%7 - 1]
        n = shiftn.get(p, {1:0, 2:0, 3:0})
        n[s] = n[s]+1
        shiftn[p] = n
        # print("{}\t{}\t{}\t{}".format(d, dow, s, p))

    # print("")
    # for p in shiftn:
    #     print("{}\t{}\t{}".format(p, sum(shiftn[p].values()), shiftn[p]))

    return work

if __name__ == "__main__":
    run("nurse.lp")
