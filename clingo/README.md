Potassco POC
============
Proof of concept solver using [patassco](https://www.potassco.org)
project.

Installation
------------

- Mac OS X: homebrew can install it: `brew install clingo`
- Windows: download the Windows
  [release](https://github.com/potassco/clingo/releases)
  from github, unzip it, and add
  the folder where you unzipped it to your path.

The goal is to be able to run it from the command line.

Testing
-------

After installing, you can use the clingo command to generate
a schedule:

```
clingo nurse.lp
```

`nurse.lp` contains all of the constraints, both hard and soft. This
also encodes who is being scheduled as well as the number of shifts
for each day, etc.

The clingo command emits a bunch of outputs. Assuming it found a
solution, each `work(...)` in the output indicate a schedule
shift. Days and shifts are 1-indexed.

To sort the output, count the persons/shifts, or however else you want
to munge the data, use the solve.py script. This will run the clingo
command and massage the output.

```
python3 solve.py
```
