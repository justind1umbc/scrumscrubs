from PyQt5.QtCore import QAbstractTableModel, Qt, QSize
from PyQt5.QtWidgets import QTableView, QTableWidget, QTableWidgetItem, QSizePolicy, QMainWindow, QAction
from PyQt5.QtGui import QIcon, QKeySequence

noshift = "--"

class ScheduleWindow(QMainWindow):
    def __init__(self, app):
        super().__init__()

        version = app.build_settings['version']
        self.setWindowTitle("ScrubSched v" + version)

        # Solve
        tbar = self.addToolBar("toolbar")
        icon = QIcon(app.get_resource("images/solve.png"))
        act = QAction(icon, "&Solve", self)
        act.setShortcuts(QKeySequence())
        act.setStatusTip("Solve scheduling problem")
        act.triggered.connect(app.solve)
        tbar.addAction(act)

        # Export toolbar icon and menu item
        fmenu = self.menuBar().addMenu("&File")
        export_act = self.makeAction(app, "images/export.png", "&Export Calendar")
        tbar.addAction(export_act)
        fmenu.addAction(export_act)
        export_act.triggered.connect(app.export)

        location_act = self.makeAction(app, "images/location.png", "&Change Location Constraints")
        tbar.addAction(location_act)
        location_act.triggered.connect(app.location)

        # Info toolbar button
        info_act = self.makeAction(app, "images/info.png", "&Info")
        info_act.triggered.connect(app.info)
        tbar.addAction(info_act)   

    def makeAction(self, app, icon_res, name):
        icon = QIcon(app.get_resource(icon_res))
        act = QAction(icon, name, self)
        return act

class ScheduleModel(QAbstractTableModel):
    def __init__(self, store, parent=None):
        super().__init__(parent)
        self.col_labels = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]
        self.store = store

        weekn = store.weekn()
        shiftn = store.maxshift()
        self.row_labels = []
        for i in range(weekn):
            for j in range(shiftn):
                self.row_labels.append("Week %d Shift %d" % (i+1,j+1))

    def rowCount(self, idx):
        """Each week in the schedule has one row in the table for each shift."""
        if idx.isValid():
            return 0
        else:
            return self.store.weekn() * self.store.maxshift()

    def columnCount(self, idx):
        """Each day of the week gets a column."""
        if idx.isValid():
            return 0
        else:
            #print("DBG (cols 8)")
            return 7

    def indexShift(self, idx):
        shiftn = self.store.maxshift()
        week = int(idx.row() / shiftn)
        dotw = idx.column()
        shift = idx.row() % shiftn
        return (7*week + dotw, shift)

    def data(self, idx, role):
        if role != Qt.DisplayRole: return None

        (d,s) = self.indexShift(idx)
        store = self.store

        if s > store.shiftn(d):
            return noshift
        else:
            who = store.lookup(d, s)
            return "" if who is None else who

    def setData(self, idx, value, role):
        if role != Qt.DisplayRole: return False

        (d,s) = self.indexShift(idx)
        store = self.store
        store.clear(d, s)
        store.assign(d, s, value)
        store.commit()

        return True

    def flags(self, index):
        if not index.isValid():
            return super().flags(index)

        flags = Qt.ItemNeverHasChildren | Qt.ItemIsEnabled
        entry = self.data(index, Qt.DisplayRole)

        if entry == "":
            # shift is empty
            return flags | Qt.ItemIsDropEnabled
        elif entry == noshift:
            # shift does not exist
            return flags ^ Qt.ItemIsEnabled
        else:
            # shift is occupied
            return flags | Qt.ItemIsSelectable | Qt.ItemIsDragEnabled | Qt.ItemIsDropEnabled

    def swapDragSource(self, source_idx):
        print("DING!")
        self.drag_source_idx = source_idx

    def dropMimeData(self, data, action, row, col, index):
        if self.drag_source_idx is not None:
            dest = self.data(index, Qt.DisplayRole)
            if dest != "" and dest is not None:
                self.setData(self.drag_source_idx, dest, Qt.DisplayRole)
        print("DONG!")
        return super().dropMimeData(data, action, row, col, index)

    def supportDropActions(self):
        return Qt.MoveAction

    def headerData(self, section, orient, role):
        if role != Qt.DisplayRole: return None
        if orient == Qt.Horizontal:
            return self.col_labels[section]
        elif orient == Qt.Vertical:
            if len(self.row_labels) == 0:
                # hack for when the row labels have not been set, yet...
                return ""
            else:
                return self.row_labels[section]
        else:
            raise Exception("orientation was not horizontal or vertical")

class ScheduleView(QTableView):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setSelectionMode(self.SingleSelection)
        self.setSelectionBehavior(self.SelectItems)

        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        self.setDragDropMode(self.DragDrop)
        self.setDropIndicatorShown(True)

        self.model = None
        self.drag_begin = None

    def setModel(self, model):
        self.model = model
        super().setModel(model)

    def startDrag(self, supportedActions):
        indICES = self.selectedIndexes()
        if len(indICES) > 0: self.drag_begin = indICES[0]
        super().startDrag(supportedActions)

    def dropEvent(self, event):
        # set the source, even if it is None
        self.model.swapDragSource(self.drag_begin)
        self.drag_begin = None
        super().dropEvent(event)

class PersonnelWidget(QTableWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setColumnCount(3)
        self.setHorizontalHeaderLabels(["Name", "Role", "Hours"])
        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Ignored)
        self.setSelectionMode(self.SingleSelection)
        self.setDragEnabled(True)
        self.rowData = []

    def sizeHint(self):
        return QSize(250, 0)

    def updateDataSet(self, dataSet):
        rows = sorted(dataSet, key=lambda trio: trio[0])
        self.setRowCount(len(rows))
        for i in range(len(rows)):
            self.setRow(i, rows[i])
        self.rowData = rows

    def setRow(self, rowi, cols):
        flags = Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemNeverHasChildren
        (p,r,h) = cols

        item = QTableWidgetItem(p)
        item.setFlags(flags | Qt.ItemIsDragEnabled)
        self.setItem(rowi, 0, item)
        item = QTableWidgetItem(r)
        item.setFlags(flags)
        self.setItem(rowi, 1, item)
        item = QTableWidgetItem(str(h))
        item.setFlags(flags)
        self.setItem(rowi, 2, item)
