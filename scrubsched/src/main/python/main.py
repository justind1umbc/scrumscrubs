import sys
import os
import sqlite3
from fbs_runtime.application_context import ApplicationContext
from PyQt5.QtWidgets import QSplitter, QWidget, QLabel, QMessageBox, QDialog, QTableWidget, QPushButton, QGridLayout, QTableWidgetItem, QFileDialog, QCalendarWidget, QVBoxLayout
from PyQt5.QtCore import Qt, QSize, QDate
from gui import ScheduleWindow, ScheduleView, ScheduleModel, PersonnelWidget
from model import ScheduleStore
import solver
from csv_exporter import export_gcal_csv as export_csv
import datetime as dt

clingo_bundled = 'clingo-5.3.0-win64'

weekn = 2
shiftreqs = [2,3,3,3,3,3,2]
# avail = sched.Schedule(weekn, max(shiftreqs))
# work = sched.WorkSchedule(weekn, shiftreqs)
# work.add(0, 0, "J")
# work.add(0, 0, "F")

work_table = [
    # week 1
    [["fiona", "justin", "miranda" ],   #SUN
     ["fiona", "justin", "miranda" ],   #MON
     ["felipe", "cameron", "fiona" ],   #TUE
     ["felipe", "cameron", "miranda"],  #WED
     ["miranda", "justin", "fiona" ],   #THU
     ["miranda", "fiona", "felipe" ],   #FRI
     ["justin", "cameron", "felipe" ]], #SAT
    [["justin", "fiona", "" ],   #SUN
     ["justin", "fiona", "miranda" ],   #MON
     ["felipe", "cameron", "fiona" ],   #TUE
     ["", "cameron", "miranda"],  #WED
     ["miranda", "fiona", "justin" ],   #THU
     ["fiona", "miranda", "felipe" ],   #FRI
     ["justin", "felipe", "cameron" ]]]

people = [("justin", "MD", 123),
          ("fiona", "Moonlighter", 23),
          ("miranda", "MD", 1024),
          ("felipe", "MD", 245),
          ("cameron", "Moonlighter", 128)]

def triplify(table):
    triples = []
    for w in range(len(table)):
        week = table[w]
        for d in range(len(week)):
            day = week[d]
            for s in range(len(day)):
                triples.append((w*7+d, s, day[s]))
    return set(triples)

class AppContext(ApplicationContext):
    def solve(self):
        # todo: check if current schedule is empty and avoid mbox if true
        if True:
            mbox = QMessageBox(self.window)
            mbox.setText("Current schedule will be discarded.")
            mbox.setInformativeText("Running the solver will overwrite "
                                    +"your current schedule. Are you sure?")
            mbox.setStandardButtons(mbox.Ok | mbox.Cancel)
            ret = mbox.exec()
        else:
            ret = QMessageBox.Ok

        if ret == QMessageBox.Ok:
            clingo_dir = '.'
            store = self.sched_store

            # try to run the bundled clingo executable if on windows
            if sys.platform == 'win32':
                p = self.get_resource(clingo_bundled)
                if os.access(p, os.F_OK):
                    clingo_dir = p

            try:
                store.empty()
                solution = solver.run(
                    clingo_dir, self.get_resource("nurse.lp"), store
                )
                self.sched_model.modelAboutToBeReset.emit()
                store.commit()
                self.sched_model.modelReset.emit()
            except:
                store.rollback()
                raise

    def export(self):
        prompt = QDialog()
        layout = QGridLayout(prompt)
        cal = QCalendarWidget()
        label = QLabel()
        label.setText('Select a day for the first shift to start (must be a Monday):')
        ok_button = QPushButton()
        ok_button.setText('OK')
        cancel_button = QPushButton()
        cancel_button.setText('Cancel')
        ok_button.setEnabled(not dt.date.today().weekday())
        
        enable_disable_ok = lambda date: ok_button.setEnabled(not date.toPyDate().weekday())
        def ok_click(_):
            prompt.close()
            self.export_to_file()
        
        def cancel_click(_):
            prompt.close()
        
        ok_button.clicked[bool].connect(ok_click)
        cancel_button.clicked[bool].connect(cancel_click)
        
        layout.addWidget(label)
        layout.addWidget(cal)
        layout.addWidget(ok_button)
        layout.addWidget(cancel_button)
        
        prompt.setWindowTitle('Starting shift date')
        
        cal.clicked[QDate].connect(enable_disable_ok)
        cal.clicked[QDate].connect(self.set_start_date)
        
        prompt.exec()
        
    def set_start_date(self, start_date):
        self.start_date = start_date.toPyDate()

    def export_to_file(self):
        file_path,_ = QFileDialog.getSaveFileName(self.window, 'Export Calendar', '.', 'CSV file (*.csv)')
        if file_path:
            if not file_path.endswith('.csv'): file_path += '.csv'

            #temporarily here
            work = self.sched_store.schedule()

            print("EXPORT!")
            export_csv(file_path, work, dt.datetime.combine(self.start_date, dt.time(7)))
        
    def info(self):
        print("INFO!")
        prompt = QDialog()
        layout = QVBoxLayout(prompt)
        self.w1 = QLabel("This is an automated scheduling software.\nClick the wand icon to have a schedule generated based on the given data.\nClick the calendar icon to export the schedule as a .csv file.\nClick the building icon to change location constraints such as shift length.\nYou can drag and drop names in the schedule to new locations also.")
        gpl_label = QLabel('''This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.''')
        
        layout.addWidget(self.w1)
        layout.addWidget(gpl_label)
        prompt.exec()
        

    def location(self):
        print("LOCATION!")
        #self.d1 = QDialog()
        d = QDialog()
        d.setMinimumWidth(2000)
        d.setMinimumHeight(1000)
        self.tableWidget = QTableWidget(d)
        self.tableWidget.setRowCount(16)
        self.tableWidget.setColumnCount(7)
        self.tableWidget.setHorizontalHeaderLabels(["SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"])
        self.tableWidget.setVerticalHeaderLabels(["Required Shifts", "Shift 1 Start", "Shift 1 End", "Shift 1 Employee Types", "Shift 2 Start", "Shift 2 End", "Shift 2 Employee Types", "Shift 3 Start", "Shift 3 End", "Shift 3 Employee Types", "Shift 4 Start", "Shift 4 End", "Shift 4 Employee Types", "Shift 5 Start", "Shift 5 End", "Shift 5 Employee Types",])
        for x in range(7):
            self.tableWidget.setItem(0,x, QTableWidgetItem("0"))
        self.tableWidget.setMinimumSize(QSize(2000,1000))
        d.setWindowTitle("Location Constraints")
        d.exec_()

    def run(self):
        window = ScheduleWindow(self)
        personnel = PersonnelWidget()
        #container = QWidget()
        splitter = QSplitter()

        db = sqlite3.connect(self.get_resource('scrubsched.db'))

        store = ScheduleStore(db)
        store.fast_forward()
        model = ScheduleModel(store)
        sched = ScheduleView(window)
        sched.setModel(model)
        self.sched_store = store
        self.sched_model = model

        personnel.updateDataSet(people)

        splitter.addWidget(sched)
        splitter.addWidget(personnel)
        #layout.setStretch(1, 0)
        #container.setLayout(layout)

        window.setCentralWidget(splitter)
        window.resize(800, 600)
        window.show()

        self.window = window
        return self.app.exec_()

if __name__ == '__main__':
    appctxt = AppContext()
    exit_code = appctxt.run()
    sys.exit(exit_code)
