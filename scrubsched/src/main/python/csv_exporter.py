import itertools
import datetime as dt
import csv

def export_gcal_csv(filename, schedule, first_shift, shift_lengths={ 2: dt.timedelta(hours=12), 3: dt.timedelta(hours=8) }, date_format='%m/%d/%Y', time_format='%I:%M %p'):
    with open(filename, 'w') as f:
        writer = csv.writer(f, quotechar='"')
        writer.writerow(['Subject', 'Start Date', 'Start Time', 'End Date', 'End Time'])

        for day, shifts in itertools.groupby(schedule, lambda t:t[0]):
            shifts = list(shifts)
            for entry in shifts:
                shift = entry[1:]
                try:
                    delta = dt.timedelta(days=day) + shift_lengths[len(shifts)]*shift[0]
                except KeyError:
                    break
                
                start = first_shift + delta
                
                writer.writerow(['Shift: ' + str(shift[1])] + list(itertools.chain.from_iterable((t.strftime(date_format), t.strftime(time_format)) for t in (start, start + shift_lengths[len(shifts)]))))

if __name__ == "__main__":
    work = [(0, 0, 'cameron'), (0, 1, 'miranda'), (1, 0, 'fiona'), (1, 1, 'justin'), (1, 2, 'felipe'), (2, 0, 'fiona'), (2, 1, 'justin'), (2, 2, 'felipe'), (3, 0, 'fiona'), (3, 1, 'justin'), (3, 2, 'miranda'), (4, 0, 'fiona'), (4, 1, 'justin'), (4, 2, 'cameron'), (5, 0, 'felipe'), (5, 1, 'miranda'), (5, 2, 'cameron'), (6, 0, 'miranda'), (6, 1, 'felipe'), (7, 0, 'cameron'), (7, 1, 'miranda'), (8, 0, 'fiona'), (8, 1, 'justin'), (8, 2, 'felipe'), (9, 0, 'fiona'), (9, 1, 'justin'), (9, 2, 'miranda'), (10, 0, 'fiona'), (10, 1, 'justin'), (10, 2, 'miranda'), (11, 0, 'fiona'), (11, 1, 'felipe'), (11, 2, 'cameron'), (12, 0, 'justin'), (12, 1, 'felipe'), (12, 2, 'miranda'), (13, 0, 'cameron'), (13, 1, 'felipe'), (14, 0, 'cameron'), (14, 1, 'miranda'), (15, 0, 'fiona'), (15, 1, 'justin'), (15, 2, 'felipe'), (16, 0, 'fiona'), (16, 1, 'justin'), (16, 2, 'felipe'), (17, 0, 'fiona'), (17, 1, 'justin'), (17, 2, 'miranda'), (18, 0, 'fiona'), (18, 1, 'justin'), (18, 2, 'cameron'), (19, 0, 'felipe'), (19, 1, 'miranda'), (19, 2, 'cameron'), (20, 0, 'miranda'), (20, 1, 'felipe'), (21, 0, 'cameron'), (21, 1, 'miranda'), (22, 0, 'fiona'), (22, 1, 'justin'), (22, 2, 'felipe'), (23, 0, 'fiona'), (23, 1, 'justin'), (23, 2, 'felipe'), (24, 0, 'fiona'), (24, 1, 'justin'), (24, 2, 'miranda'), (25, 0, 'fiona'), (25, 1, 'felipe'), (25, 2, 'cameron'), (26, 0, 'felipe'), (26, 1, 'miranda'), (26, 2, 'cameron'), (27, 0, 'miranda'), (27, 1, 'justin')]
    export_gcal_csv("test.csv", work, dt.datetime(2019, 4, 28))

#gcal csv format:
#Subject,Start Date,Start Time,End Date,End Time,All Day Event,Description,Location,Private
#Subject: string
#Date: 05/30/2020
#Time: 1:00 PM
#All day: True/False
#Description: string
#Location: string
#Private: True/False
