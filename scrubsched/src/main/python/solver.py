import subprocess as subproc
import re
import os
from tempfile import NamedTemporaryFile
from string import Template

empty_value = "nobody"

def tablize(work):
    sched = [(int(d), int(s), p) for (d,s,p) in work]
    table = []
    maxweek = 1+max([int(d/7) for (d,_,_) in sched])
    maxshift = 1+max([int(s) for (_,s,_) in sched])

    for y in range(maxweek*maxshift):
        table.append(["" for _ in range(7)])
    for (d,s,p) in sched:
        w = int(d/7)
        table[w*maxshift + s][d % 7] = p
    return table

def stats(work):
    shiftn = {}
    #print("DAY\tDOW\tSHIFT\tPERSON")
    for w in work:
        (d, s, p) = w
        d = int(d)
        s = int(s)
        # dow = days[d%7 - 1]
        n = shiftn.get(p, {0:0, 1:0, 2:0})
        n[s] = n[s]+1
        shiftn[p] = n
        # print("{}\t{}\t{}\t{}".format(d, dow, s, p))

    # print("")
    # for p in shiftn:
    #     print("{}\t{}\t{}".format(p, sum(shiftn[p].values()), shiftn[p]))

def run(clingo_dir, script_path, store):
    proc = subproc.run("clingo "+script_path,
                       stdout=subproc.PIPE,
                       cwd=clingo_dir,
                       shell=True,
                       encoding="utf8")

    work = []
    workre = 'work\((\d+),(\d+),([^)]+)\)'
    lines = proc.stdout.split("\n")
    for i in range(len(lines)):
        if not re.match('Answer: (\d+)', lines[i]): continue
        work = [m.groups() for m in re.finditer(workre, lines[i+1])]

    # Print out schedule for debug trace, while developing...
    assert len(work) > 0, "failed to extract answer"
    work = [(int(d), int(s), (None if e == empty_value else e))
            for (d,s,e) in work]
    work = sorted(work, key=lambda x: (x[0], x[1]))
    print(work)

    # and again.
    days = "SUN MON TUE WED THU FRI SAT".split(" ")
    table = tablize(work)
    print("\t".join(days))
    for col in table: print("\t".join(col))

    # XXX: hack to update the number of weeks
    maxd = max(d for (d,_,_) in work)
    store.set_weekn(int((maxd+1) / 7))
    for (d,s,e) in work: store.assign(d, s, e)

def logic_script(params):
    person_params = params['persons']
    sched_params = params['shifts']
    personids = set(x[0] for x in person_params)

    # %:- work(_,_,fiona).
    # %:~ work(D,_,justin), D\7+1=4. [100]
    # %:~ work(_,1,justin). [100]
    # %:~ work(D,_,miranda), D\7+1=1. [100]

    prefs = []
    # loop over each: person, day index, shift index, day of the week, weight
    for (p, d, s, dow, w) in person_params:
        line = ""
        if d is not None and s is not None:
            line = '%:~ work({1},{2},{0}). [{3}]'.format(p, d, s, w)
        elif dow is not None and s is not None:
            line = '%:~ work(D,{2},{0}), D\\7+1={1}. [{3}]'.format(p, dow, s, w)
        elif dow is not None:
            line = '%:~ work(D,_,{0}), D\\7+1={1}. [{2}]'.format(p, dow, w)
        elif d is not None:
            line = '%:~ work({1},_,{0}). [{2}]'.format(p, d, w)
        elif s is not None:
            line = '%:~ work(_,{1},{0}). [{2}]'.format(p, s, w)
        else:
            continue
        prefs.append(line)

    return """
{people}
person(0).
{weeks}
week(1..W) :- weeks(W).
day(0..(7*W)-1) :- weeks(W). % 0-SUN ... 6-SAT
dayshifts((0;6), 0..1). % SUN,SAT have 2 shifts per day
dayshifts(1..5, 0..2). % MON-FRI have 3 shifts per day
shift(D,S) :- day(D), dayshifts(D\\7,S).

% work/4 has an extra week parameter.
work(D/7+1,D,S,P) :- work(D,S,P), week(D/7+1).

% -- Generate ----------------------------------------------------------------
% (one person for each shift)
{{ work(D,S,P) : person(P) }} = 1 :- shift(D,S).

morning(D,S) :- day(D-1), shift(D,S), not shift(D,S-1).
graveyard(D,S) :- day(D), shift(D,S), not shift(D,S+1).

% -- Test --------------------------------------------------------------------
% never work >1 shifts in a day
:- {{ work(D,S,P) }} > 1, day(D), person(P), P!=nobody.
% never work morning after a graveyard shift
:- work(D,S,P), work(D+1,0,P), graveyard(D,S), person(P), day(D+1), P!=nobody.
% maximum of {shifts_per_week} shifts per week
:- {shifts_per_week} < {{ work(W,D,S,P) : shift(D,S) }}, person(P), week(W).
% try to schedule people for the same shift each time
:~ not work(D1,S,P), not work(D2,S,P), shift(D1,S), shift(D2,S), D1!=D2, person(P). [3]
% try not to schedule people for very different shifts (i.e. morning/graveyard)
% BROKEN
%:~ work(D1,S1,P), work(D2,S2,P), shift(D1,S1), shift(D2,S2), abs(S2-S1)=2, D1!=D2, person(P). [1]
% try not to schedule nobody
:~ work(_,_,0). [500]
% try to schedule fairly

% Preferences are weak conditions
{preferences}

#show work/3.
""".format(weeks = 'weeks({}).'.format(sched_params['week_count']),
           people = 'person({}).'.format(";".join([str(x) for x in personids])),
           preferences = "\n".join(prefs),
           shifts_per_week = sched_params['per_week'])

def solve(clingo_dir, params):
    tmp = NamedTemporaryFile(mode='w', encoding='utf8', delete=False)
    tmp.write(logic_script(params))
    tmp.close()
    ret = run(clingo_dir, tmp.name)
    os.unlink(tmp.name)
    return ret

if __name__ == '__main__':
    params = {
        'persons': [
            (1, None, None, None, None),
            (2, None, None, None, None),
            (3, None, 1, None, 500),
            (4, None, None, None, None),
            (5, None, None, None, None)
        ],
        'shifts': {
            'week_count': 4,
            'per_week': 4
        }
    }
    solve('.', params)

