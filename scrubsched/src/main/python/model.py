class ScheduleStore:
    def __init__(self, db):
        self.db = db
        self.schedule_id = None

    def commit(self):
        self.db.commit()

    def rollback(self):
        self.db.rollback()

    def fast_forward(self):
        row = self.db.execute('SELECT scheduleID FROM schedule ORDER BY startDate DESC').fetchone()
        self.schedule_id = None if row is None else row[0]

    def schedule(self):
        c = self.db.execute("""
        SELECT dayIndex, shiftIndex, employeeID FROM ScheduledShifts
        """)
        return c.fetchall()

    def swap(self, day1, shift1, day2, shift2):
        c = self.db.execute("""
        SELECT employeeID FROM ScheduledShifts AS ss
        WHERE ss.dayIndex = ? AND ss.shiftIndex = ? AND ss.scheduleID = ?
        """, (day1, shift1, self.schedule_id))
        employee1 = c(0)

        self.db.execute("""
        UPDATE ScheduledShifts SET employeeID = (
          SELECT employeeID FROM ScheduledShifts
          WHERE dayIndex = ? AND shiftIndex = ? and scheduleID = ?
        ) WHERE dayIndex = ? AND shiftIndex = ? and scheduleID = ?
        """, (day2, shift2, self.schedule_id, day1, shift1, self.schedule_id))

        self.db.execute("""
        UPDATE ScheduledShifts SET employeeID = ? AND dayIndex = ?
        AND shiftIndex = ? and scheduleID = ?
        """, (employee1, day2, shift2, self.schedule_id))

    def assign(self, day, shift, employee):
        if employee is None:
            empid = None
        else:
            row = self.db.execute("SELECT employeeId FROM Employee WHERE name = ?",
                                  (employee,)).fetchone()
            if row is None: raise RuntimeError("unknown employee: {}".format(employee))
            empid = row[0]

        self.db.execute("""
        INSERT INTO ScheduledShifts (dayIndex, shiftIndex, employeeID, scheduleID)
        VALUES (?, ?, ?, ?)
        """, (day, shift, empid, self.schedule_id))

    def clear(self, day, shift):
        """Removes any employee from the schedule on day/shift.
        """
        self.db.execute("""
        DELETE FROM ScheduledShifts WHERE dayIndex = ? AND shiftIndex = ?
        AND scheduleID = ?
        """, (day, shift, self.schedule_id))

    def empty(self):
        """Empties the schedule. Deletes all ScheduledShifts assigned to a
        ScheduleID.
        """
        self.db.execute("DELETE FROM ScheduledShifts WHERE scheduleID = ?",
                        (self.schedule_id,))

    def lookup(self, day, shift):
        r = self.db.execute("""
        SELECT e.name
        FROM ScheduledShifts AS ss
        JOIN Employee AS e ON e.employeeID = ss.employeeID
        WHERE ss.dayIndex = ? AND ss.shiftIndex = ? AND ss.scheduleID = ?
        """, (day, shift, self.schedule_id)).fetchone()
        return None if r is None else r[0]

    def weekn(self):
        return self.db.execute("""
        SELECT weekCount FROM Schedule WHERE scheduleID = ?
        """, (self.schedule_id,)).fetchone()[0]

    def set_weekn(self, weekn):
        self.db.execute("""
        UPDATE Schedule SET weekCount = ? WHERE scheduleID = ?
        """, (weekn, self.schedule_id))

    def shiftn(self, day_index):
        dow = 1 + day_index%7
        return self.db.execute("""
        SELECT numRequiredShifts FROM RequiredShifts WHERE dayIndex = ?
        """, (dow,)).fetchone()[0]

    def maxshift(self):
        return self.db.execute("""
        SELECT MAX(numRequiredShifts) FROM RequiredShifts
        """).fetchone()[0]

class ConstraintStore:
    def __init__(self, db):
        self.db = db

    def commit(self):
        self.db.commit()

    def rollback(self):
        self.db.rollback()

class EmployeeStore:
    def __init__(self, db):
        self.db = db

    def commit(self):
        self.db.commit()

    def rollback(self):
        self.db.rollback()

    def emp_dict(self, row):
        return dict(zip(['id', 'name', 'hours', 'shifts'], row))
