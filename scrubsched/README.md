Scrum Scrubs Scheduler
======================

Copied from [fbs tutorial](https://github.com/mherrmann/fbs-tutorial).
Install `fbs` and run:

```
# Install fbs, PyQt, PyInstaller:
pip3 install fbs PyQt5==5.9.2 PyInstaller==3.4
# Start app
fbs run
```

Or with virtualenv if you cannot install modules globally:

```
# Setup venv in root dir:
python3 -m venv venv
# On Mac/Linux:
source venv/bin/activate
# On Windows:
call venv\scripts\activate.bat
# Install fbs, PyQt, PyInstaller:
pip3 install fbs PyQt5==5.9.2 PyInstaller==3.4
# Start app
fbs run
```
