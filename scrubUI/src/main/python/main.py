from fbs_runtime.application_context import ApplicationContext, cached_property
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import sys

class AppContext(ApplicationContext):
    def run(self):
        stylesheet = self.get_resource('styles.qss')
        self.app.setStyleSheet(open(stylesheet).read())
        self.window.show()
        return self.app.exec_()
    @cached_property
    def window(self):
        return MainWindow()

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        text = QLabel()
        text.setWordWrap(True)
        text.setText("Choose an option:")
        button = QPushButton('Generate Schedule')
        button2 = QPushButton('Add/Remove/Modify Staff')
        button3 = QPushButton('Add/Remove/Modify Scheduling Constraints')
        button4 = QPushButton('Help')
        button4.clicked.connect(getHelp)
        layout = QVBoxLayout()
        layout.addWidget(text)
        layout.addWidget(button)
        layout.addWidget(button2)
        layout.addWidget(button3)
        layout.addWidget(button4)
        self.setLayout(layout)

def getHelp():
    d = QDialog()
    b1 = QPushButton("ok",d)
    b1.move(50,50)
    d.setWindowTitle("Dialog")
    d.setWindowModality(Qt.ApplicationModal)
    d.exec_()

if __name__ == '__main__':
    appctxt = AppContext()
    exit_code = appctxt.run()
    sys.exit(exit_code)
